const { Schema, model } = require('mongoose');

const UserSchema = Schema({

    name:{
        type: String,
        require: [true, 'The name is required'],
        trim: true // delete spaces blank
    },
    lastName:{
        type: String,
        require: [true, 'The lastname is required'],
        trim: true
    },
    dni:{
        type: String,
        require: [true, 'The DNI is required'],
        trim: true
    },
    cellNumber:{
        type: String,
        trim: true
    },
    email:{
        type: String,
        require: [true, 'The email is required'],
        trim: true ,
        unique: true
    },
    password:{
        type: String,
        require: [true, 'The password is required'],
        trim: true
    },
    
    role: {
        type: String,
        required: true,
        
    },
    //enum: ['ADMIN_ROLE', 'USER_ROLE']
    img: {
        type: String,
    },

    estado:{
        type: Boolean,
        default: true
    },

    google: {
        type: Boolean,
        default: false
    },

    // This field is added automatically
    createdAt:{
        type: Date,
        default: Date.now()
    }

});

// custom Output (JSON) not print __v and password
UserSchema.methods.toJSON = function(){
    const {__v, password, _id, ...user } = this.toObject();
    user.uid = _id;
    return user;
}

module.exports = model('User', UserSchema);