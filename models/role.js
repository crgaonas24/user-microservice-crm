const {Schema, model} = require('mongoose');

const SchemaRole = Schema({
    role :{
        type: String,
        require: [true, 'The role is requiere!']
    }
})
module.exports = model('Role',SchemaRole);