const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');


class Server {

    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.usersPath = '/api/v1/users';
        this.authPath = '/api/v1/auth';

        // Database connection
        this.connectionDB();

        // Middleswares
        this.middlewares();

        // Routing
        this.routes(); // Initialize routes
    }

    async connectionDB(){
        await dbConnection();
    }

    // Methods
    middlewares(){

        // CORS
        this.app.use(cors());

        // data JSON Format
        this.app.use(express.json());

        // public directory
        this.app.use(express.static('public'));

    }

    // Define routes
    routes() {
        this.app.use(this.authPath, require('../routes/authRoute'))
       this.app.use(this.usersPath, require('../routes/userRoute'))
    }

    // port of execution
    listen (){
        this.app.listen(this.port, () =>{
            console.log('Server running on port ', this.port);
        });
        
    }

}


module.exports = Server;