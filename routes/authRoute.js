const { Router }= require('express');
const { check } = require('express-validator');
const { login } = require('../controllers/authController');


// Invoke custom middleware        
const { fieldsValidate } = require('../middlewares/fields-validation');

// Routing
const router = Router();

// routes instances | field validation
router.post('/login',[
    check('email', 'The email is requiered!').isEmail(),
    check('password', 'The password is requiered!').not().isEmpty(),
    fieldsValidate
], login);


module.exports = router;
