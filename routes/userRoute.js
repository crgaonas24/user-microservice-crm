const { Router }= require('express');
const { check } = require('express-validator');

// Invoke controllers
const { getUsers, 
        putUsers, 
        postUsers, 
        deleteUsers, 
        patchUsers } = require('../controllers/usersController');
// Invoke custom middleware  
const{
        fieldsValidate,
        validateJWT,
        adminRole,
        haveRole
} = require('../middlewares');     


// Invoke helpers functions 
const { isValidRole, emailExists, userById } = require('../helpers/db-validators');

// Routing
const router = Router();

// routes instances
router.get('/', getUsers);

router.put('/:id',[
        check('id', 'It is not a valid id').isMongoId(),
        check('id').custom(userById),
        check('role').custom( (role) => isValidRole(role) ),
        fieldsValidate
], putUsers);

router.post('/',[
        check('name', 'The Name is required!').not().isEmpty(),
        check('lastName', 'The Last Name is required!').not().isEmpty(),
        check('dni', 'The DNI must have 10 characters!').isLength({min:10}),
        check('password', 'The Password must have more than 6 characters!').isLength({min:6}),
        check('email', 'The Email is not valid!').isEmail(),
        check('email').custom(emailExists),
        check('role').custom( (role) => isValidRole(role) ),
        //check('role', 'Role not valid!').isIn(['ADMIN_ROLE', 'USER_ROLE']),
        fieldsValidate        
],     postUsers);

router.delete('/:id', [
        validateJWT,
        //adminRole,
        haveRole('ADMIN_ROLE', 'SALES_ROLE'),
        check('id', 'It is not a valid id').isMongoId(),
        check('id').custom(userById),
        fieldsValidate 
],
deleteUsers);

router.patch('/', patchUsers);

module.exports = router;

