const mongoose = require('mongoose');

// database connection
const dbConnection = async() =>{
    try {
        await mongoose.connect( process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false

        });

        console.log('successful database connection')


    } catch (error) {
        console.log(error)
        throw new Error('Oh something went wrong! Cannot initialize database')
    }

}

module.exports ={
    dbConnection
}