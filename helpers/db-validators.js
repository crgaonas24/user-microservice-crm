// Invoke custon model role
const Role = require("../models/role");
const User = require("../models/user");

const isValidRole = async (role = "") => {
  const roleExist = await Role.findOne({ role });
  //console.info(role)
  if (!roleExist) {
    throw new Error(`The ${role} role is not registered in the database`);
  }
};

// Validate email
const emailExists = async (email = "") => {
  const emailExist = await User.findOne({ email: email });
  if (emailExist) {
    throw new Error(`This email: ${email} is already registered`);
  }
};


// Validate user by Id
const userById = async (id) => {
    const userExist = await User.findById( id );
    if (!userExist) {
      throw new Error(`This ID: ${id} not exist`);
    }
  };


module.exports = {
  isValidRole,
  emailExists,
  userById
};
