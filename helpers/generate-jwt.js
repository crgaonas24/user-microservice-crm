const jwt = require('jsonwebtoken')

const generateJWT = ( uid = '', role, email) =>{
    return  new Promise((resolve, reject) =>{
        const payLoad = { uid, role, email};
        jwt.sign(payLoad, process.env.SECRETEORPRIVATEKEY, {
            expiresIn: '4h'
        }, (err, token)=>{
            if (err){
                console.log(err);
                reject( 'Not generate TOKEN for user')
            }else{
                resolve (token);
            }
        }
        
        )
    })
}

module.exports = {
    generateJWT
}