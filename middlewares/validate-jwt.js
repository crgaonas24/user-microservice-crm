const { response } = require('express');
const jwt =  require('jsonwebtoken');
const User = require('../models/user');

const validateJWT  = async (req, res = response, next)=>{

    const token = req.header('x-token');

    if (!token){
        return res.status(401).json({
            msg: 'The token is required!'
        })
    }

    try {

        // Id of token
        const {uid} = jwt.verify(token, process.env.SECRETEORPRIVATEKEY);

        // search Id on the database 
        const user = await User.findById(uid);

        if(!user){
            return res.status(401).json({
                msg: 'User does not exist in the database'
            })
        }

        // check if user with state false
        if (!user.estado){
            return res.status(401).json({
                msg: 'The token not valid'
            })
        }

        // pass user to request
        req.user = user;
    
        next();

    } catch (error) {
        console.log(error);
        res.status(401).json({
            msg: 'The token not valid!'
        })
    }

    

}


module.exports = {
    validateJWT
}