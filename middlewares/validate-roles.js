const { response } = require("express");

const adminRole = (req, res = response, next) =>{

    if(!req.user){
        return res.status(500).json({
            msg: 'it is about verifying the role without validating the first token'
        })
    }
    const { role, name} = req.user;

    if(role !== 'ADMIN_ROLE'){
        return res.status(401).json({
            msg: `${name}, you do not have sufficient permissions for delete this user`
        });
       
    }
    next();

}

const haveRole = (...roles)=>{
    return (req, res = response, next)=>{
        req.user.role;

        if(!req.user){
            return res.status(500).json({
                msg: 'it is about verifying the role without validating the first token'
            })
        }

        if (!roles.includes(req.user.role)){
            return res.status(401).json({
                msg: `requierd this rols: ${roles}`
            });
        }
        next();
    }
}

module.exports = {
    adminRole,
    haveRole
}