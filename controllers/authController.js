const { response } = require("express");
const User = require('../models/user');
const bcryptjs = require('bcryptjs');
const { generateJWT } = require("../helpers/generate-jwt");

const login = async (req, res = response)=>{

    const {email, password} = req.body;

    try {

        // Check if the mail exists
        const user = await User.findOne({ email });
        if(!user){
            return res.status(400).json({
                msg: 'User / Password are incorrets!'
            })

        }

        // Check if state of user is active or true
        if(!user.estado){
            return res.status(400).json({
                msg: 'User / Password are incorrets or inactive user!'
            })

        }
        // Check password
        const validatePassword =  bcryptjs.compareSync (password, user.password);
        if(!validatePassword){
            return res.status(400).json({
                msg: 'User / Password are incorrets or inactive user!'
            })
        }
        
        // Generate JWT
        const token = await generateJWT( user.id, user.role, user.email);

        res.json({
            msg: 'Login OK',
            user,
            token
        })
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Something went wrong try later'
        })
    }

  

}


module.exports ={
    login
}