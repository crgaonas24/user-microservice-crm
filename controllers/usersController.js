const { response, query } = require('express');
const User = require('../models/user')
const bcryptjs = require('bcryptjs');

const getUsers = async (req, res = response) => {
   
    const query = {estado : true};
   
    const { limite =5, from = 0 } = req.query;
    
    // Destructuring Arrays
    const [total, users] = await Promise.all([
        User.countDocuments(query),
        User.find( query)
            .skip(Number(from))
            .limit(Number(limite))
    ])

    // send data in JSON format 
    res.json({
        total,
        users
    });
}

const putUsers = async (req, res= response) =>  {

    const id = req.params.id;

    // Destructuring | exclude data
    const {_id, password, google, email, ...resto}= req.body;

    if ( password ){
        // Encrypt password
        const salt = bcryptjs.genSaltSync();
        resto.password = bcryptjs.hashSync(password, salt);
    }

    const user = await User.findByIdAndUpdate ( id, resto);

    //TODO: vALIDAR CONTRA LA BASE DE DATOS
    // send data in JSON format
    res.json({
        user
    })
}


const postUsers = async (req, res= response) =>  {

    // Destructuring
    const { name, lastName, dni, email, cellNumber, password, role, estado, img, google }= req.body;
    const user = new User( {name, lastName, dni, email, cellNumber, password, role, estado, img, google} );
    
    // Encrypt password
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);

    // Save data
    await user.save();

    // Return data in JSON format
    res.json({
        user
    })
}

const deleteUsers = async (req, res= response) =>  {

    // Id user delete
    const { id } = req.params;

    // Delete user
    //const user = await User.findByIdAndDelete(id);
 
    // Retrieve authenticated user
    const userAuth = req.user;
    
    // Update user state
    const user = await User.findByIdAndUpdate(id, {estado: false} );
    res.json({
        msg: 'User deleted',
        user,
        userAuth 
    })
}

const patchUsers = async (req, res= response) =>  {

    res.json({
        msg: 'Patch API'
    })
}

module.exports = {
    getUsers,
    putUsers,
    postUsers,
    deleteUsers,
    patchUsers
}