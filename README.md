# User Microservice

This microservices allows you to create users, assign roles and handle authentication

## Install dependences
``` npm install ```

## Dependences
### Server and manage environment variables
``` npm i express dotenv ```

## Cors
``` npm i cors ```

## Mongoose
``` npm i mongoose ```

## Bcryptjs
```npm i bcryptjs```

## Express-validator
``` npm i express-validator ```

## JWT
``` npm i jsonwebtoken ```